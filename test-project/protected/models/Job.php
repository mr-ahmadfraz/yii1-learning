<?php

/**
 * This is the model class for table "tbl_job".
 *
 * The followings are the available columns in table 'tbl_job':
 * @property string $job_id
 * @property string $job_title
 * @property string $job_skill
 * @property string $job_description
 * @property string $job_location
 * @property string $created_date
 * @property integer $min_salary
 * @property integer $max_salary
 * @property integer $is_active
 * @property string $company
 */
class Job extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_job';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('min_salary, max_salary, is_active', 'numerical', 'integerOnly'=>true),
			array('job_title', 'length', 'max'=>100),
			array('job_skill', 'length', 'max'=>500),
			array('job_description', 'length', 'max'=>1000),
			array('job_location, company', 'length', 'max'=>200),
			array('created_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('job_id, job_title, job_skill, job_description, job_location, created_date, min_salary, max_salary, is_active, company', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'job_id' => 'Job',
			'job_title' => 'Job Title',
			'job_skill' => 'Job Skill',
			'job_description' => 'Job Description',
			'job_location' => 'Job Location',
			'created_date' => 'Created Date',
			'min_salary' => 'Min Salary',
			'max_salary' => 'Max Salary',
			'is_active' => 'Is Active',
			'company' => 'Company',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('job_id',$this->job_id,true);
		$criteria->compare('job_title',$this->job_title,true);
		$criteria->compare('job_skill',$this->job_skill,true);
		$criteria->compare('job_description',$this->job_description,true);
		$criteria->compare('job_location',$this->job_location,true);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('min_salary',$this->min_salary);
		$criteria->compare('max_salary',$this->max_salary);
		$criteria->compare('is_active',$this->is_active);
		$criteria->compare('company',$this->company,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Job the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
