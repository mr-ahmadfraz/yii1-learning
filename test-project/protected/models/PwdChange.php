<?php

class PwdChange extends CFormModel
{
	
	public $old_password;
	public $new_password;
	public $repeat_password;
	
	
	//Define the rules for old_password, new_password and repeat_password with changePwd Scenario.
	
	public function rules()
	{
	  return array(
		array('old_password, new_password, repeat_password', 'required', 'on' => 'changePwd'),
		array('old_password', 'findPasswords', 'on' => 'changePwd'),
		array('repeat_password', 'compare', 'compareAttribute'=>'new_password', 'on'=>'changePwd'),
	  );
    }
	
	
	//matching the old password with your existing password.
	public function findPasswords($attribute, $params)
	{
		$user= Yii::app()->user->name;
			$dataDb = Yii::app()->db->createCommand()
			->select('password')
			->from('tbl_user')
			->where('username=:username', array(':username'=>$user))
			->queryRow();
			if ($dataDb['password'] != $this->old_password)
				$this->addError($attribute, 'Old password is incorrect.');
	}
	
}//end class