<?php
/* @var $this JobController */
/* @var $dataProvider CActiveDataProvider */
/* @var $pagination CPagination */


$data=$dataProvider->model;

//Job objects array
$record=$dataProvider->getData();


$this->breadcrumbs=array(
	'Jobs',
);

$this->menu=array(
	array('label'=>'Create Job', 'url'=>array('create')),
	array('label'=>'Manage Job', 'url'=>array('admin')),
);
?>

<h1>Jobs</h1>

<!--	Field Names	-->
<div class="container">

		<div class="row">
			<?php
				$cssClass="col-lg py-2 bg-primary text-white border-start border-white";//div class attribute value
				
				
				echo "<div class='".$cssClass."'><b>".CHtml::encode($data->getAttributeLabel('job_id'))."</b><br/></div>";
				
				echo "<div class='".$cssClass."'><b>".CHtml::encode($data->getAttributeLabel('job_title'))."</b><br/></div>";
				
				echo "<div class='".$cssClass."'><b>".CHtml::encode($data->getAttributeLabel('job_skill'))."</b><br/></div>";
				
				echo "<div class='".$cssClass."'><b>".CHtml::encode($data->getAttributeLabel('job_location'))."</b><br/></div>";
				
				echo "<div class='".$cssClass."'><b>".CHtml::encode($data->getAttributeLabel('is_active'))."</b><br/></div>";
				
				echo "<div class='".$cssClass."'><b>".CHtml::encode($data->getAttributeLabel('created_date'))."</b><br/></div>";
				
			/*
				echo "<div class='".$cssClass."'><b>".CHtml::encode($data->getAttributeLabel('min_salary'))."</b><br/></div>";
				
				echo "<div class='".$cssClass."'><b>".CHtml::encode($data->getAttributeLabel('max_salary'))."</b><br/></div>";
				
				echo "<div class='".$cssClass."'><b>".CHtml::encode($data->getAttributeLabel('job_description'))."</b><br/></div>";
				
				echo "<div class='".$cssClass."'><b>".CHtml::encode($data->getAttributeLabel('company'))."</b><br/></div>";
			*/
				echo "<div class='".$cssClass."'><br/></div>";//empty column for buttons view and edit
			?>
		</div><!--  row  -->

		
	
	<!--.................................. 	Records Listing	 -->
<?php

//pagination Logic to show records according to page size in JobController
$offset=$pagination->offset;
$limit=$pagination->limit;
$total=$dataProvider->totalItemCount;
if(!($limit>$total))
{
	$nextLimitCounter=$limit;
}else
{
	$nextLimitCounter=$total;
}
if($offset>=$limit && !($offset>=$total))
	$nextLimitCounter=$offset+$limit;
for($i=$offset;$i<$nextLimitCounter;$i++)
{
	if(!($i>=$total))
	{
?>

		<!--    Generating Record Row        -->
		<div class="row joblisting-row border-bottom border-2 border-white">
			<?php
		
				echo "<div class='col-lg py-2'>".CHtml::encode($record[$i]->job_id)."<br /></div>";
				
				echo "<div class='col-lg py-2'>".CHtml::encode($record[$i]->job_title)."<br /></div>";
				
				echo "<div class='col-lg py-2'>".CHtml::encode($record[$i]->job_skill)."<br /></div>";
				
				echo "<div class='col-lg py-2'>".CHtml::encode($record[$i]->job_location)."<br /></div>";
				
				echo "<div class='col-lg py-2'>".CHtml::encode($record[$i]->is_active)."<br /></div>";
				
				echo "<div class='col-lg py-2'>".CHtml::encode($record[$i]->created_date)."<br /></div>";
				
			/*	
				echo "<div class='col-lg py-2'>".CHtml::encode($record[$i]->min_salary)."<br /></div>";
								
				echo "<div class='col-lg py-2'>".CHtml::encode($record[$i]->max_salary)."<br /></div>";
				
				echo "<div class='col-lg py-2'>".CHtml::encode($record[$i]->job_description)."<br /></div>";
				
				echo "<div class='col-lg py-2'>".CHtml::encode($record[$i]->company)."<br /></div>";
			*/
			
				//View and update links for record
				echo "<div class='col-lg'>".CHtml::link(CHtml::image(Yii::app()->request->baseUrl.'/images/view.png'), array('view', 'id'=>$record[$i]->job_id)).CHtml::link(CHtml::image(Yii::app()->request->baseUrl.'/images/update.png'), array('update', 'id'=>$record[$i]->job_id))."<br /></div>";
			?>
			
		
			
		</div><!--  row  -->
		
		
<?php
	}//if end

}//for loop end

//.......................................................default widget to list data

/*<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>*/


//----------------------------------------- Generating page navigation according to $pagination
	$this->widget('CLinkPager', array('pages' => $pagination,));
?>		

		
	</div><!--container-->


