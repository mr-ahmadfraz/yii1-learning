<?php
/* @var $this JobController */
/* @var $model Job */

$this->breadcrumbs=array(
	'Jobs'=>array('index'),
	$model->job_id,
);

$this->menu=array(
	array('label'=>'List Job', 'url'=>array('index')),
	array('label'=>'Create Job', 'url'=>array('create')),
	array('label'=>'Update Job', 'url'=>array('update', 'id'=>$model->job_id)),
	array('label'=>'Delete Job', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->job_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Job', 'url'=>array('admin')),
);
?>

<h1>View Job #<?php echo $model->job_id; ?></h1>
<table style="border:1px solid blue;">
<?php foreach($model as $key=>$value){?>
<tr>
	<td style="background-color:#cef5cb;text-align:right;text-transform:capitalize;"><?php echo $key;?></td>
	<td style="background-color:#c4dbf2">->
		<?php 
			if($key == 'is_active')
			{
				if($value==0)
				{
					echo "Inactive";
				}else
				{
					echo "active";
				}
			}else
			{
				echo $value;
			}
				
			
		?>
	</td>
</tr>
<?php } ?>
</table>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'job_id',
		'job_title',
		'job_skill',
		'job_description',
		'job_location',
		'created_date',
		'min_salary',
		'max_salary',
		'is_active',
		'company',
	),
)); ?>
